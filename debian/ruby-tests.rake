require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = FileList['./spec/**/*_spec.rb'] - FileList[
  './spec/celluloid/io/reactor_spec.rb',
  './spec/celluloid/io/tcp_socket_spec.rb',
  './spec/celluloid/io/unix_socket_spec.rb',
  './spec/celluloid/io/mailbox_spec.rb']
end
